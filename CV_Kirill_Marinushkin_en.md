![photo](https://gitlab.com/kmarinushkin/cv/raw/master/Kirill_Marinushkin_photo2.png)

# Kirill Marinushkin

Embedded Software Developer

## Contacts

* Address:  31134, Hildesheim, Germany; looking for a relocation to Berlin, Germany
* Phone:    +4915787189615
* E-mail:   [k.marinushkin@gmail.com](mailto:k.marinushkin@gmail.com)
* GitHub:   [https://github.com/kmarinushkin](https://github.com/kmarinushkin)
* LinkedIn: [https://de.linkedin.com/in/kirill-marinushkin-4562268a](https://de.linkedin.com/in/kirill-marinushkin-4562268a)

## Education

Master of Technics and Technology / Magister für Technik und Technologie

St. Petersburg Electrotechnical University 2006-2012

## Maintaining

* in Linux upstream: maintaining a driver for sound codec pcm3060

## Contributions

* Linux: sound subsystem
* ALSA (Advanced Linux Sound Architecture): alsa-lib, alsa-utils
* OpenSSL

## Certificates

* Master of Technology - St. Petersburg Electrotechnical University 2006-2012
* Brainbench C Master certificate - Transcript ID# 12420980
  (valid Jan 2015 - Jan 2018) [link](http://brainbench.com/transcript.jsp?pid=12420980)

## Skills and experience

#### 1. Automotive Linux distributions:

* GENIVI - develop kernelspace and userspace components
* AGL - integrate userspace components

#### 2. Embedded Linux HW platforms:

* arm core -  develop for SoC by Freescale/NXP, Variscite
* arm64 core - develop for SoC by Renesas
* x86_64 core - develop for SoC by Intel

#### 3. Build environments:

* Yocto - adapt, write, debug, improve bitbake recipes, to integrate OSS and
  custom components

#### 4. Linux kernelspace:

* maintaining an upstream driver for sound codec pcm3060
* contribute
* use advanced debugging techniques
* develop for subsystems: sound, crypto API, security keys, usb

#### 5. Linux userspace:

* C, C++ (up to, and including, C++11)
* design patterns - use with C++
* unit testing, test-driven development
* objdump analyzing
* gdb, gdb-server
* makefiles, automake
* advanced security tools - netfilters, OpenSSL, SSH, SFTP, stunnel
* systemd - write and configure services, optimize boot performance
* LXC - write, improve templates and configs for Linux containers
* databases - use SQLite, MySQL
* scripting - bash, python

#### 6. U-Boot - develop custom features

#### 7. Embedded systems without OS:

* cores
  * ARM Cortex-M0/3/4 by NXP, TI, STM
  * DSP by Microchip
  * 8-bit, 16-bit PIC by Microchip
  * MSP430 by TI
* C, PIC Assembler
* firmware update - develop custom mechanisms
* networking - integrate protocols HTTP, WebSockets and SSL over uIP library
* USB - develop support of profiles HID, UAC / UAC2
* other interfaces - including CAN, I2S, I2C, LonWorks, Sub-GHz radio

#### 8. Other development tools:

* git
* static code analysis
* doxygen code documenting
* UML design
* bug-tracking
* continues integration

## Languages

* English (fluent)
* German (B1 certified, currently learning)

## Work Experience

#### 1. ADIT - joint venture company of Robert Bosch GmbH and DENSO Corporation

July 2015 - till now

Software Engineer

I am developing Embedded Linux systems for automotive.

Responsibilities: design, integration, coding, unit testing, optimization,
support, documentation.

Projects:

* Integration improvement - I improve bitbake recipies to optimize the delivery
* OSS integration - I intergrate userspace OSS components
* Boot performance optimization - I optimize systemd configuration to achieve
  boot speed improvement
* Security features - I developed kernelspace and userspace solutions which
  use HW-specific mechanisms to achieve security features on different
  platforms

#### 2. Argus-Spectr

July 2011 - July 2015

Embedded Software Engineer

I was developing Embedded Linux systems, as well as low-level embedded
systems for security sensors networks.

Responsibilities: design, coding, unit testing, support.

Projects:

* Municipal emergency notifications system - I developed several Embedded
  Linux systems as a part of city-level system for monitoring and notifying
  about emergency situations
* Production and quality control automation - for production and quality
  control automation in the company, I suggested and applied several
  modifications into the SW/HW solution for increasing efficiency of this
  process
* Sensors networks - I developed several low-level devices based on PIC,
  ARM Cortex-M3, DSP cores for wired and wireless security sensors networks
* Embedded network - I integrated several protocols including HTTP,
  WebSocket, SSL for embedded systems based on ARM Cortex-M3 core

#### 3. Green Design International

September 2010 - June 2011

Software Engineer (working student)

I was developing and integrating web-services.

Responsibilities: design, coding, support.

Projects:

* Shop-rent - I developed several web solutions into the service for
  "renting" online shops

## About me

* I am efficient. I know how to find optimal ways to solve problems.
* I learn fast. When a new topic comes - I know how to quick-dive into it.
* I am initiative. If I find a problem - I will suggest a solution for it.
